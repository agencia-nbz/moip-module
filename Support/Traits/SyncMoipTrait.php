<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 9/4/18
 * Time: 9:36 AM
 */

namespace Modules\Moip\Support\Traits;


use Modules\Moip\Entities\Customer\CustomerTrait;

trait SyncMoipTrait
{

    public function syncMoip() : self
    {
        $method = null;

        $data = $this->getUpdated();

        $methodName = $this->nameTaskByStatus($data->status);

        if ( !env('MOIP_TEST', false) ) {
            $onSync = $this->getObjectOnSync();
            $onSync->customTaskAnyStatus();
            $onSync->$methodName();
        }

        return $this;

    }

    protected function nameTaskByStatus(string $status) : string
    {
        $methodTask = null;
        foreach (explode('_', $status) as $data ) {
            $methodTask .= ucfirst(strtolower($data));
        }
        return ("customTask" . $methodTask . "Status");
    }

}
