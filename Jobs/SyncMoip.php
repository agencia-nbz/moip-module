<?php

namespace Modules\Moip\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Moip\Support\Interfaces\SyncMoipInterface;

class SyncMoip implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $resource;

    public function __construct(SyncMoipInterface $resource)
    {
        $this->resource = $resource;
    }

    public function handle()
    {
        $this->resource->syncMoip();
    }
}
