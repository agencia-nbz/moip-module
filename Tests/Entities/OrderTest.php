<?php

namespace Modules\Moip\Tests\Entities;


use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Entities\Order\OrderInterface;
use Modules\Moip\Entities\Order\OrderTrait;

class OrderTest extends Model implements OrderInterface
{
    use OrderTrait;

    protected $fillable = [
        'id', 'shipping', 'addition', 'discount', 'customer', 'items',
    ];

    public static function faker() : self
    {
        return self::make([
            'id' => rand(),
            'shipping' => rand(0, 10000),
            'addition' => rand(0, 10000),
            'discount' => rand(0, 10),
            'customer' => CustomerTest::faker(),
            'items' => collect(array_fill(0, rand(1, 10), ItemTest::faker())),
        ]);
    }
}
