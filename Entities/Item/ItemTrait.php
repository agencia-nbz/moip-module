<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/21/19
 * Time: 7:20 AM
 */

namespace Modules\Moip\Entities\Item;


use Illuminate\Support\Str;

trait ItemTrait
{

    protected $moipNameAttribute = "name";
    protected $moipQuantityAttribute = "quantity";
    protected $moipDetailAttribute = "detail";
    protected $moipPriceAttribute = "price";

    public function getMoipName() : string
    {
        $attribute = $this->moipNameAttribute;
        return Str::limit($this->$attribute, 247);
    }

    public function getMoipQuantity() : int
    {
        $attribute = $this->moipQuantityAttribute;
        return intval($this->$attribute);
    }

    public function getMoipDetail() : string
    {
        $attribute = $this->moipDetailAttribute;
        return Str::limit($this->$attribute, 247);
    }

    public function getMoipPrice() : int
    {
        $attribute = $this->moipPriceAttribute;
        return intval($this->$attribute * 100);
    }

}
