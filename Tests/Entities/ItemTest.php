<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/24/19
 * Time: 3:52 PM
 */

namespace Modules\Moip\Tests\Entities;


use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Entities\Item\ItemInterface;
use Modules\Moip\Entities\Item\ItemTrait;

class ItemTest extends Model implements ItemInterface
{
    use ItemTrait;

    public $fillable = [
        'name', 'detail', 'price', 'quantity',
    ];

    public static function faker() : self
    {
        $faker = Factory::create('pt_BR');
        return self::make([
            'name' => $faker->sentence(3, true),
            'detail' => $faker->sentence(5, true),
            'price' => rand(100, 10000),
            'quantity' => 1,
        ]);
    }
}
