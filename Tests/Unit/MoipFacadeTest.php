<?php

namespace Modules\Moip\Tests\Unit;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Modules\Moip\Support\Facades\MoipOAuth;
use Modules\Moip\Support\Facades\MoipBasicAuth;
use Modules\Moip\Support\Facades\MoipRequest;
use Moip\Moip;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MoipFacadeTest extends TestCase
{
    public function testMoipOAuth()
    {
        $this->assertTrue(is_a(resolve('moip-oauth'), Moip::class), "O Facade OAuth não é do tipo Moip");
    }

    public function testMoipBasicAuth()
    {
        $this->assertTrue(is_a(resolve('moip-basic-auth'), Moip::class), "O Facade BasicAuth não é do tipo Moip");
    }

    public function testMoipRequest()
    {
        $this->assertTrue(is_a(resolve('moip-request'), Client::class), "O Facade MoipRequest não é do tipo Guzzle");
    }
}
