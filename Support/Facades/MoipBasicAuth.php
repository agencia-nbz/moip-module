<?php

namespace Modules\Moip\Support\Facades;


use Illuminate\Support\Facades\Facade;

class MoipBasicAuth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'moip-basic-auth';
    }
}
