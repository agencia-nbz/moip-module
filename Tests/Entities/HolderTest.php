<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/22/19
 * Time: 8:28 PM
 */

namespace Modules\Moip\Tests\Entities;


use Illuminate\Database\Eloquent\Model;
use Faker\Factory;
use Modules\Moip\Entities\Customer\CustomerInterface;
use Modules\Moip\Entities\Customer\CustomerTrait;
use Carbon\Carbon;
use Modules\Moip\Entities\Holder\HolderInterface;
use Modules\Moip\Entities\Holder\HolderTrait;

class HolderTest extends Model implements HolderInterface
{
    use HolderTrait;

    protected $faker;

    protected $fillable = [
        'phone', 'address',
        'name', 'birth', 'tax', 'phone',
    ];

    public static function faker() : self
    {
        $faker = Factory::create('pt_BR');
        return self::make([
            'name' => $faker->name,
            'birth' => Carbon::parse($faker->dateTimeThisCentury->format('Y-m-d')),
            'tax' => $faker->cpf(false),
            'address' => AddressTest::faker(),
            'phone' => PhoneTest::faker(),
        ]);
    }

}
