<?php

namespace Modules\Moip\Console;

use Illuminate\Console\Command;
use Modules\Moip\Entities\Notification\Notification;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Moip\Support\Facades\MoipOAuth;

class MoipNotificationStart extends Command
{
    protected $signature = 'moip:create-notification {url}';

    protected $description = 'Create Notifications to App Listen Moip.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $notification = Notification::saveMoip($this->argument('url'));
        $this->info("Notification ID#{$notification->id} {$notification->code} created with success!");

    }

    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
