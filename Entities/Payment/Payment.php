<?php

namespace Modules\Moip\Entities\Payment;

use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Entities\Order\Order;
use Modules\Moip\Support\Facades\MoipOAuth;
use Modules\Moip\Support\Interfaces\OnSyncMoipInterface;
use Modules\Moip\Support\Interfaces\SyncMoipInterface;
use Modules\Moip\Support\Traits\SyncMoipTrait;

class Payment extends Model implements SyncMoipInterface
{
    use SyncMoipTrait;

    protected $fillable = [
        'code', 'cache', 'order_id',
    ];

    protected $table = "moip_module_payments";

    final public function getUpdated()
    {
        $this->cache = json_encode($this->getConnectionMoip());
        $this->save();
        return $this->getCached();
    }

    final public function getCached()
    {
        return json_decode($this->cache);
    }

    final public function getConnectionMoip() : \Moip\Resource\Payment
    {
        return MoipOAuth::payments()->get($this->code);
    }

    final public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getObjectOnSync(): OnSyncMoipInterface
    {
        return $this->order->getObjectOnSync();
    }
}
