<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 9/4/18
 * Time: 9:36 AM
 */

namespace Modules\Moip\Support\Interfaces;


interface OnSyncMoipInterface
{

    public function customTaskAnyStatus();
    public function customTaskCreatedStatus();
    public function customTaskWaitingStatus();
    public function customTaskPaidStatus();
    public function customTaskNotPaidStatus();
    public function customTaskRevertedStatus();
    public function customTaskInAnalysisStatus();
    public function customTaskPreAuthorizedStatus();
    public function customTaskAuthorizedStatus();
    public function customTaskCancelledStatus();
    public function customTaskRefundedStatus();
    public function customTaskReversedStatus();
    public function customTaskSettledStatus();

}
