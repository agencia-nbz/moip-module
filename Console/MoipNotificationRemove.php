<?php

namespace Modules\Moip\Console;

use Illuminate\Console\Command;
use Modules\Moip\Entities\Notification\Notification;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Moip\Support\Facades\MoipOAuth;

class MoipNotificationRemove extends Command
{
    protected $signature = 'moip:remove-notification {id?}';

    protected $description = 'Remove Notifications to App Listen Moip.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        if ( $this->argument('id') ) {
            $notification = Notification::find('id');
            !is_null($notification) ? $notification->clearMoip() : null;
        } else {
            Notification::all()->each(function ($notification) {
               $notification->clearMoip();
            });
        }

        $this->info("Notification Deleted!");

    }

    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
