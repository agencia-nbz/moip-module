<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/22/19
 * Time: 8:28 PM
 */

namespace Modules\Moip\Tests\Entities;


use Illuminate\Database\Eloquent\Model;
use Faker\Factory;
use Modules\Moip\Entities\Customer\CustomerInterface;
use Modules\Moip\Entities\Customer\CustomerTrait;
use Carbon\Carbon;

class CustomerTest extends Model implements CustomerInterface
{
    use CustomerTrait;

    protected $faker;

    protected $fillable = [
        'id', 'phone', 'billing', 'shipping',
        'name', 'email', 'birth', 'tax', 'phone',
    ];

    public static function faker() : self
    {
        $faker = Factory::create('pt_BR');
        return self::make([
            'id' => rand(),
            'name' => $faker->name,
            'email' => $faker->safeEmail(),
            'birth' => Carbon::parse($faker->dateTimeThisCentury->format('Y-m-d')),
            'tax' => $faker->cpf(false),
            'billing' => AddressTest::faker(),
            'shipping' => AddressTest::faker(),
            'phone' => PhoneTest::faker(),
        ]);
    }

}
