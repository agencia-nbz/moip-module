<?php

namespace Modules\Moip\Entities\Order;


use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Entities\Payment\Payment;
use Modules\Moip\Support\Facades\MoipOAuth;
use Modules\Moip\Support\Interfaces\OnSyncMoipInterface;
use Modules\Moip\Support\Interfaces\SyncMoipInterface;
use Modules\Moip\Support\Traits\SyncMoipTrait;

class Order extends Model implements SyncMoipInterface
{
    use SyncMoipTrait;

    protected $fillable = [
        'code', 'cache',
    ];

    protected $table = "moip_module_orders";

    final public static function getList()
    {
        return call_user_func_array([MoipOAuth::orders(), "getList"], func_get_args());
    }

    final public function getUpdated()
    {
        $this->cache = json_encode($this->getConnectionMoip());
        $this->save();
        $this->updatedPaymentLast();

        return $this->getCached();
    }

    final public function updatedPaymentLast()
    {
        if ( $payment = $this->payments->last() )
            return $payment->getUpdated();
    }

    final public function getCached()
    {
        return json_decode($this->cache);
    }

    final public function getConnectionMoip() : \Moip\Resource\Orders
    {
        return MoipOAuth::orders()->get($this->code);
    }

    final public function orderable()
    {
        return $this->morphTo();
    }

    final public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    final public function savePay(\Moip\Resource\Payment $payment) : Payment
    {
        $data['cache'] = json_encode($payment);
        $data['code'] = json_decode($data['cache'])->id;
        $payment = Payment::make($data);
        $this->payments()->save($payment);

        return $payment;
    }

    public function getObjectOnSync(): OnSyncMoipInterface
    {
        return $this->orderable;
    }
}
