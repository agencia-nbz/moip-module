<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:44 PM
 */

namespace Modules\Moip\Entities\Address;

trait AddressTrait
{

    protected $moipStreetAttribute = "street";
    protected $moipStreetNumberAttribute = "number";
    protected $moipComplementAttribute = "complement";
    protected $moipDistrictAttribute = "district";
    protected $moipCityAttribute = "city";
    protected $moipStateAttribute = "state";
    protected $moipZipAttribute = "zip";

    public function getMoipStreet() : string
    {
        $attribute = $this->moipStreetAttribute;
        return $this->$attribute;
    }

    public function getMoipStreetNumber() : int
    {
        $attribute = $this->moipStreetNumberAttribute;
        return intval($this->$attribute);
    }

    public function getMoipComplement() : string
    {
        $attribute = $this->moipComplementAttribute;
        return $this->$attribute;
    }

    public function getMoipDistrict() : string
    {
        $attribute = $this->moipDistrictAttribute;
        return $this->$attribute;
    }

    public function getMoipCity() : string
    {
        $attribute = $this->moipCityAttribute;
        return $this->$attribute;
    }

    public function getMoipState() : string
    {
        $attribute = $this->moipStateAttribute;
        return substr(trim(strtoupper($this->$attribute)), 0, 2);
    }

    public function getMoipZip() : string
    {
        $attribute = $this->moipZipAttribute;
        return preg_replace("/[^0-9]/", "", substr(trim($this->$attribute), 0, 20));
    }
}
