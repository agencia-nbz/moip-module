<?php

namespace Modules\Moip\Tests\Unit;

use Carbon\Carbon;
use Modules\Moip\Entities\Address\AddressTrait;
use Faker\Factory;
use Modules\Moip\Entities\Customer\Customer;
use Modules\Moip\Tests\Entities\AddressTest;
use Modules\Moip\Tests\Entities\PhoneTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerTest extends TestCase
{

    use RefreshDatabase;

    protected $customer;
    protected $faker;

    public function testMoipCreate()
    {
        $this->customer = \Modules\Moip\Tests\Entities\CustomerTest::faker();
        $this->assertTrue(is_a($this->customer->saveMoip(), Customer::class), "Retorno não é do Tipo Resorce Customer Moip");
    }


}
