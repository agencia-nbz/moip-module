<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/26/19
 * Time: 8:23 PM
 */

namespace Modules\Moip\Entities\Holder;

use Carbon\Carbon;
use Modules\Moip\Entities\Address\AddressInterface;
use Modules\Moip\Entities\Phone\PhoneInterface;
use Modules\Moip\Tests\Entities\AddressTest;
use Moip\Resource\Holder;

interface HolderInterface
{
    public function getMoipFullName() : string;

    public function getMoipBirthdate() : Carbon;

    public function getMoipTaxDocument() : string;

    public function getMoipPhone() : PhoneInterface;

    public function getMoipAddress() : AddressInterface;

    public function createMoip() : Holder;

}
