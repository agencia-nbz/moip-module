<?php

namespace Modules\Moip\Entities\Order;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Modules\Moip\Entities\Customer\CustomerInterface;
use Modules\Moip\Entities\Holder\HolderInterface;
use Modules\Moip\Entities\Item\ItemInterface;
use Moip\Resource\Orders;
use Modules\Moip\Support\Facades\MoipOAuth as Moip;
use Modules\Moip\Entities\Payment\Payment;

trait OrderTrait
{

    protected $moipShippingPriceAttribute = "shipping";
    protected $moipIdAttribute = "id";
    protected $moipAdditionPriceAttribute = "addition";
    protected $moipDiscountPriceAttribute = "discount";
    protected $moipItemsAttribute = "items";
    protected $moipCustomerAttribute = "customer";

    public function getMoipId() : int
    {
        $attribute = $this->moipIdAttribute;
        return intval($this->$attribute);
    }

    public function getMoipShippingPrice() : int
    {
        $attribute = $this->moipShippingPriceAttribute;
        return intval($this->$attribute);
    }

    public function getMoipAdditionPrice() : int
    {
        $attribute = $this->moipAdditionPriceAttribute;
        return intval($this->$attribute);
    }

    public function getMoipDiscountPrice() : int
    {
        $attribute = $this->moipDiscountPriceAttribute;
        return intval($this->$attribute * 100);
    }

    public function getMoipItems() : Collection
    {
        $attribute = $this->moipItemsAttribute;
        return $this->$attribute;
    }

    public function getMoipCustomer() : CustomerInterface
    {
        $attribute = $this->moipCustomerAttribute;
        return $this->$attribute;
    }

    final protected function createMoip() : Orders
    {
        try {
            $order = Moip::orders()->setOwnId(uniqid());
            $this->getMoipItems()->each(function (ItemInterface $item) use ($order) {
                $order->addItem($item->getMoipName(), $item->getMoipQuantity(), $item->getMoipDetail(), $item->getMoipPrice());
            });
            $order->setShippingAmount($this->getMoipShippingPrice())
                ->setAddition($this->getMoipAdditionPrice())
                ->setDiscount($this->getMoipDiscountPrice())
                ->setCustomer($this->getMoipCustomer()->getMoip()->getConnectionMoip())
                ->create();
        } catch (\Exception $e) {
            dd($e);
        }

        return $order;
    }

    final public function moipOrder()
    {
        return $this->morphOne(Order::class, 'orderable');
    }

    final public function saveMoip() : Order
    {
        $data['cache'] = json_encode($this->createMoip());
        $data['code'] = json_decode($data['cache'])->id;
        $order = Order::make($data);
        $this->moipOrder()->save($order);

        return $order;
    }

    final public function payMoipCreditCard(HolderInterface $holder, string $hash, string $description, int $installment = 1) : Payment
    {
            $order = $this->moipOrder ?? $this->saveMoip();
            $payment = $order->getConnectionMoip()->payments()->setCreditCardHash($hash, $holder->createMoip())
                ->setInstallmentCount($installment)
                ->setStatementDescriptor(Str::limit($description,10))
                ->execute();
        return $order->savePay($payment);
    }

    final public function payMoipBoleto(Carbon $expirationDate, array $instructions = []) : Payment
    {
            $order = $this->moipOrder ?? $this->saveMoip();
            $payment = $order->getConnectionMoip()->payments()
                ->setBoleto($expirationDate, env('MOIP_LOGO_BOLETO_URL'), array_replace(["", "", ""], $instructions))->execute();
        return $order->savePay($payment);
    }
}
