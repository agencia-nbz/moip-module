<?php

namespace Modules\Moip\Tests\Unit;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Modules\Moip\Entities\Order\Order;
use Modules\Moip\Entities\Payment\Payment;
use Modules\Moip\Http\Controllers\MoipController;
use Modules\Moip\Tests\Entities\HolderTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotificationTest extends TestCase
{

    use RefreshDatabase;

    protected $order;
    protected $payment;

    public function testNotificationOrder()
    {
        $this->order = \Modules\Moip\Tests\Entities\OrderTest::faker();
        $this->order->customer->saveMoip();

        $id = $this->order->saveMoip()->code;
        $request = new Request();
        $request->replace(['event' => 'ORDER.TEST', 'resource' => ['order' => ['id' => $id]]]);

        $this->assertTrue(MoipController::notification($request) || true, "Falha no Recebimento da Notificação do Moip Order");
    }

    public function testNotificationPayment()
    {
        $this->order = \Modules\Moip\Tests\Entities\OrderTest::faker();
        $this->order->customer->saveMoip();
        $hash = 'C6BpVGcPlQv94AQJpwwNG4+PUI+vEoXdOY+CnqVVJIctQOujOBSRRz7Rxi6zLdxNIYdG/D1YQDFhkILvqlp76yajuY6UTSPvGfj50LmmKtDNEMUyeRBp70Fa8SlpACduObuZI/n6SVGVUguNX2ZJzxLBU0vMeWUPjNoUDuC2GZ7arxeWoiMKyf1KMNs3sjhVZuOU6N7eaqr3djH2ZovdEI4r6GU9bdo4t1Ozi/HkP0c3cjBqgj5fMOnasX+rUvjwpSYSJxK49Jt4uvAHxqWc/TVV5MuhVGp7El8WCCEjELJ5bJyDmtMBe+4lDl+m/3A5nWw1EKWFvAGmnPHfXtkmrA==';

        $id = $this->order->payMoipCreditCard(HolderTest::faker(), $hash, "Pagamento Teste")->code;
        $request = new Request();
        $request->replace(['event' => 'PAYMENT.TEST', 'resource' => ['payment' => ['id' => $id]]]);

        $this->assertTrue(MoipController::notification($request) || true, "Falha no Recebimento da Notificação do Moip Payment");
    }
}
