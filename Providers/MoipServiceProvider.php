<?php

namespace Modules\Moip\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Moip\Console\MoipAppStart;
use Modules\Moip\Console\MoipNotificationRemove;
use Modules\Moip\Console\MoipNotificationStart;
use Modules\Moip\Console\StartNotificationsMoip;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;

class MoipServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->commands([
            MoipAppStart::class,
            MoipNotificationStart::class,
            MoipNotificationRemove::class,
        ]);

        App::bind('moip-basic-auth', function () {
            return new Moip(new BasicAuth(env('MOIP_TOKEN'), env('MOIP_KEY')), env('MOIP_SANDBOX', true) ? Moip::ENDPOINT_SANDBOX : Moip::ENDPOINT_PRODUCTION);

        });

        App::bind('moip-oauth', function () {
            return new Moip(new OAuth(env('MOIP_ACCESS_TOKEN')), env('MOIP_SANDBOX', true) ? Moip::ENDPOINT_SANDBOX : Moip::ENDPOINT_PRODUCTION);

        });

        App::bind('moip-request', function () {
            return new Client(['base_uri' => env('MOIP_SANDBOX', true) ? Moip::ENDPOINT_SANDBOX : Moip::ENDPOINT_PRODUCTION]);
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('moip.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'moip'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/moip');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/moip';
        }, \Config::get('view.paths')), [$sourcePath]), 'moip');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/moip');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'moip');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'moip');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
