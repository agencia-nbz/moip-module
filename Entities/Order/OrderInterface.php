<?php

namespace Modules\Moip\Entities\Order;

use Illuminate\Support\Collection;
use Modules\Moip\Entities\Holder\HolderInterface;
use Modules\Moip\Entities\Payment\Payment;
use Carbon\Carbon;

interface OrderInterface
{

    public function getMoipId() : int;

    public function getMoipShippingPrice() : int;

    public function getMoipAdditionPrice() : int;

    public function getMoipDiscountPrice() : int;

    public function getMoipItems() : Collection;

    public function getMoipCustomer();

    public function payMoipCreditCard(HolderInterface $holder, string $hash, string $description, int $installment = 1) : Payment;

    public function payMoipBoleto(Carbon $expirationDate, array $instructions) : Payment;
}
