<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/18/19
 * Time: 4:16 PM
 */

namespace Modules\Moip\Support\Facades;


use Illuminate\Support\Facades\Facade;

class MoipOAuth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'moip-oauth';
    }

}
