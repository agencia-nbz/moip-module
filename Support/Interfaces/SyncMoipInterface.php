<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 9/4/18
 * Time: 9:36 AM
 */

namespace Modules\Moip\Support\Interfaces;


interface SyncMoipInterface
{

    public function syncMoip();

    public function getObjectOnSync() : OnSyncMoipInterface;

}
