<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/21/19
 * Time: 7:20 AM
 */

namespace Modules\Moip\Entities\Item;


interface ItemInterface
{

    public function getMoipName() : string;

    public function getMoipQuantity() : int;

    public function getMoipDetail() : string;

    public function getMoipPrice() : int;

}
