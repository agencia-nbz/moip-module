<?php

namespace Modules\Moip\Entities\Notification;

use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Support\Facades\MoipOAuth;

class Notification extends Model
{
    protected $fillable = ['code', 'cache'];

    protected $table = "moip_module_notifications";

    final public static function saveMoip(string $url) : Notification
    {
        $data['cache'] = json_encode(self::createMoip($url));
        $data['code'] = json_decode($data['cache'])->id;
        return self::create($data);
    }

    final protected static function createMoip($url)
    {
        try {
            return MoipOAuth::notifications($url)->addEvent('ORDER.*')
                ->addEvent('PAYMENT.*')
                ->setTarget($url)
                ->create();
        } catch (\Exception $e) {
            dd($e);
        }
    }

    final public function clearMoip()
    {
        MoipOAuth::notifications()->delete($this->code);
        $this->delete();
    }
}
