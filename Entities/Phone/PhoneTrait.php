<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:44 PM
 */

namespace Modules\Moip\Entities\Phone;

trait PhoneTrait
{

    protected $moipAreaCodeAttribute = "area";
    protected $moipPhoneNumberAttribute = "number";

    public function getMoipAreaCode() : int
    {
        $attribute = $this->moipAreaCodeAttribute;
        return intval($this->$attribute);
    }

    public function getMoipPhoneNumber() : int
    {
        $attribute = $this->moipPhoneNumberAttribute;
        return intval($this->$attribute);
    }

}
