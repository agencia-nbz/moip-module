<?php

namespace Modules\Moip\Tests\Unit;

use Illuminate\Support\Carbon;
use Modules\Moip\Entities\Order\Order;
use Modules\Moip\Entities\Payment\Payment;
use Modules\Moip\Tests\Entities\HolderTest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{

    use RefreshDatabase;

    protected $order;

    public function testMoipCreate()
    {
        $this->order = \Modules\Moip\Tests\Entities\OrderTest::faker();
        $this->order->customer->saveMoip();
        $this->assertTrue(is_a($this->order->saveMoip(), Order::class), "Retorno não é do Tipo Resorce Order Moip");
    }

    public function testPayBoleto()
    {
        $this->order = \Modules\Moip\Tests\Entities\OrderTest::faker();
        $this->order->customer->saveMoip();
        $this->assertTrue(is_a($this->order->payMoipBoleto(Carbon::now()->addDays(3), ["LINHA 1 DE TESTE", "LINHA 2 DE TESTE", "LINHA 3 DE TESTE"]), Payment::class), "Retorno não é do Tipo Resorce Payment Módulo");
    }

    public function testPayCreditCard()
    {
        $this->order = \Modules\Moip\Tests\Entities\OrderTest::faker();
        $this->order->customer->saveMoip();
        $hash = 'C6BpVGcPlQv94AQJpwwNG4+PUI+vEoXdOY+CnqVVJIctQOujOBSRRz7Rxi6zLdxNIYdG/D1YQDFhkILvqlp76yajuY6UTSPvGfj50LmmKtDNEMUyeRBp70Fa8SlpACduObuZI/n6SVGVUguNX2ZJzxLBU0vMeWUPjNoUDuC2GZ7arxeWoiMKyf1KMNs3sjhVZuOU6N7eaqr3djH2ZovdEI4r6GU9bdo4t1Ozi/HkP0c3cjBqgj5fMOnasX+rUvjwpSYSJxK49Jt4uvAHxqWc/TVV5MuhVGp7El8WCCEjELJ5bJyDmtMBe+4lDl+m/3A5nWw1EKWFvAGmnPHfXtkmrA==';
        $this->assertTrue(is_a($this->order->payMoipCreditCard(HolderTest::faker(), $hash, "Pagamento Teste"), Payment::class), "Retorno não é do Tipo Resorce Payment Módulo");
    }

    public function testPayBoletoWithoutInstructions()
    {
        $this->order = \Modules\Moip\Tests\Entities\OrderTest::faker();
        $this->order->customer->saveMoip();
        $this->assertTrue(is_a($this->order->payMoipBoleto(Carbon::now()->addDays(3)), Payment::class), "Retorno não é do Tipo Resorce Payment Módulo");
    }
}
