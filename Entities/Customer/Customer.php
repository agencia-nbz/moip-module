<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:43 PM
 */

namespace Modules\Moip\Entities\Customer;


use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Support\Facades\MoipOAuth;
use Modules\Moip\Support\Interfaces\OnSyncMoipInterface;
use Modules\Moip\Support\Interfaces\SyncMoipInterface;
use Modules\Moip\Support\Traits\SyncMoipTrait;

class Customer extends Model
{

    protected $fillable = [
        'code', 'cache',
    ];

    protected $table = "moip_module_customers";

    final public function getUpdated()
    {
        $this->cache = json_encode($this->getConnectionMoip());
        $this->save();
        return $this->getCached();
    }

    final public function getCached()
    {
        return json_decode($this->cache);
    }

    final public function getConnectionMoip() : \Moip\Resource\Customer
    {
        return MoipOAuth::customers()->get($this->code);
    }

    final public function customerable()
    {
        return $this->morphTo();
    }

}
