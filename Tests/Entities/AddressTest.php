<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/22/19
 * Time: 8:29 PM
 */

namespace Modules\Moip\Tests\Entities;

use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Entities\Address\AddressInterface;
use Modules\Moip\Entities\Address\AddressTrait;

class AddressTest extends Model implements AddressInterface
{

    use AddressTrait;

    protected $fillable = [
        'street', 'number', 'complement', 'district',
        'city', 'state', 'zip',
    ];

    public static function faker() : self
    {
        $faker = Factory::create('pt_BR');
        return self::make([
            'street' => $faker->streetName,
            'number' => $faker->buildingNumber,
            'complement' => $faker->secondaryAddress,
            'district' => $faker->streetSuffix,
            'city' => $faker->city,
            'state' => $faker->stateAbbr,
            'zip' => preg_replace("/[^0-9]/", "", $faker->postcode)
        ]);
    }

}
