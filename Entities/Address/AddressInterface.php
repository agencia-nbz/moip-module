<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:44 PM
 */

namespace Modules\Moip\Entities\Address;


use Carbon\Carbon;

interface AddressInterface
{
    public function getMoipStreet() : string;

    public function getMoipStreetNumber() : int;

    public function getMoipComplement() : string;

    public function getMoipDistrict() : string;

    public function getMoipCity() : string;

    public function getMoipState() : string;

    public function getMoipZip() : string;

}
