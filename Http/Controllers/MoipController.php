<?php

namespace Modules\Moip\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Moip\Entities\Order\Order;
use Modules\Moip\Entities\Payment\Payment;
use Modules\Moip\Jobs\SyncMoip;

class MoipController extends Controller
{
    public static function notification(Request $request)
    {
        $resource = self::getResource($request->toArray());
        if ( $resource ) {
            dispatch(new SyncMoip($resource));
        }
    }

    protected static function getResource(array $data)
    {
        $resource = $data['event'];
        $resource = explode('.', $resource);

        switch ($resource[0])
        {
            case 'ORDER':
                $resource = Order::where('code', $data['resource']['order']['id'])->first();
                break;
            case 'PAYMENT':
                $resource = Payment::where('code', $data['resource']['payment']['id'])->first();
                break;
            default:
                $resource = null;
        }

        return $resource;
    }
}
