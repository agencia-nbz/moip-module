<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:44 PM
 */

namespace Modules\Moip\Entities\Customer;


use Carbon\Carbon;
use Modules\Moip\Entities\Address\AddressInterface;
use Modules\Moip\Entities\Phone\PhoneInterface;
use Nexmo\Call\Collection;

interface CustomerInterface
{
    public function getMoipFullName() : string;

    public function getMoipEmail() : string;

    public function getMoipBirthDate() : Carbon;

    public function getMoipTaxDocument() : string;

    public function getMoipBillingAddress() : AddressInterface;

    public function getMoipShippingAddress() : AddressInterface;

    public function getMoipPhone() : PhoneInterface;

    public function getMoip() : Customer;

}
