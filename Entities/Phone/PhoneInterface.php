<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:44 PM
 */

namespace Modules\Moip\Entities\Phone;


interface PhoneInterface
{
    public function getMoipAreaCode() : int;

    public function getMoipPhoneNumber() : int;

}
