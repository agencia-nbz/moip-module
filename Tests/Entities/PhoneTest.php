<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/22/19
 * Time: 8:46 PM
 */

namespace Modules\Moip\Tests\Entities;

use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Modules\Moip\Entities\Phone\PhoneInterface;
use Modules\Moip\Entities\Phone\PhoneTrait;

class PhoneTest extends Model implements PhoneInterface
{

    use PhoneTrait;

    protected $fillable = [
        'area', 'number'
    ];

    public static function faker() : self
    {
        $faker = Factory::create('pt_BR');
        return self::make([
            'area' => $faker->areaCode,
            'number' => $faker->cellphone(false),
        ]);
    }
}
