<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/26/19
 * Time: 8:22 PM
 */

namespace Modules\Moip\Entities\Holder;

use Carbon\Carbon;
use Modules\Moip\Entities\Address\AddressInterface;
use Modules\Moip\Entities\Phone\PhoneInterface;
use Moip\Resource\Holder;
use Modules\Moip\Support\Facades\MoipOAuth as Moip;

trait HolderTrait
{

    protected $moipFullNameAttribute = "name";
    protected $moipBirthDateAttribute = "birth";
    protected $moipTaxDocumentAttribute = "tax";

    protected $moipAddressAttribute = "address";
    protected $moipPhoneAttribute = "phone";

    public function getMoipFullName() : string
    {
        $attribute = $this->moipFullNameAttribute;
        return substr(trim($this->$attribute), 0, 90);
    }

    public function getMoipBirthDate() : Carbon
    {
        $attribute = $this->moipBirthDateAttribute;
        return Carbon::parse($this->$attribute);
    }

    public function getMoipTaxDocument() : string
    {
        $attribute = $this->moipTaxDocumentAttribute;
        return substr(trim(str_replace(['-', '.'], '', $this->$attribute)), 0, 11);
    }

    public function getMoipAddress() : AddressInterface
    {
        $attribute = $this->moipAddressAttribute;
        return $this->$attribute;
    }

    public function getMoipPhone() : PhoneInterface
    {
        $attribute = $this->moipPhoneAttribute;
        return $this->$attribute;
    }

    public function createMoip() : Holder
    {
        return Moip::holders()->setFullname($this->getMoipFullName())
            ->setBirthDate($this->getMoipBirthDate()->format('Y-m-d'))
            ->setTaxDocument($this->getMoipTaxDocument(), 'CPF')
            ->setPhone($this->getMoipPhone()->getMoipAreaCode(), $this->getMoipPhone()->getMoipPhoneNumber(), 55)
            ->setAddress('BILLING',
                $this->getMoipAddress()->getMoipStreet(),
                $this->getMoipAddress()->getMoipStreetNumber(),
                $this->getMoipAddress()->getMoipDistrict(),
                $this->getMoipAddress()->getMoipCity(),
                $this->getMoipAddress()->getMoipState(),
                $this->getMoipAddress()->getMoipZip(),
                $this->getMoipAddress()->getMoipComplement());
    }
}
