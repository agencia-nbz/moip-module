<?php

namespace Modules\Moip\Console;

use Illuminate\Console\Command;
use Modules\Moip\Support\Facades\MoipRequest;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MoipAppStart extends Command
{
    protected $name = 'moip:app';

    protected $description = 'Generate an App Moip';

    protected $body;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $data = $this->sendRequest();
            $this->setEnv("MOIP_APP", $data->id);
            $this->setEnv("MOIP_ACCESS_TOKEN", $data->accessToken);
            $this->setEnv("MOIP_SECRET", $data->secret);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function sendRequest()
    {
        return json_decode(MoipRequest::post(
            'v2/channels',
            [
                'headers' => [
                    'Accept'     => 'application/json',
                ],
                'json' => [
                    'name' => env('MOIP_APP_NAME'),
                    'description' => env('MOIP_APP_DESCRIPTION'),
                    'site' => env('APP_URL'),
                    'redirectUri' => env('MOIP_REDIRECT'),
                ],
                'auth' => [ env('MOIP_TOKEN'), env('MOIP_KEY')],
            ]
        )->getBody()->getContents());
    }

    public function setEnv($envKey, $envValue)
    {
        $envFile = app()->environmentFilePath();

        $str = file_get_contents($envFile);

        $oldValue = env($envKey);


        $str = str_replace("{$envKey}={$oldValue}", "{$envKey}={$envValue}", $str);

        $fp = fopen($envFile, 'w');
        $this->info("Set {$envKey} to {$envValue}");
        fwrite($fp, $str);
        fclose($fp);
    }


}
