<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 2/19/19
 * Time: 8:44 PM
 */

namespace Modules\Moip\Entities\Customer;

use Carbon\Carbon;
use Modules\Moip\Entities\Phone\PhoneInterface;
use Modules\Moip\Support\Facades\MoipOAuth as Moip;
use Modules\Moip\Entities\Address\AddressInterface;
use Nexmo\Call\Collection;

trait CustomerTrait
{

    protected $moipIdAttribute = "id";

    protected $moipFullNameAttribute = "name";
    protected $moipEmailAttribute = "email";
    protected $moipBirthDateAttribute = "birth";
    protected $moipTaxDocumentAttribute = "tax";

    protected $moipBillingAddressAttribute = "billing";
    protected $moipShippingAddressAttribute = "shipping";
    protected $moipPhoneAttribute = "phone";

    public function getMoipFullName() : string
    {
        $attribute = $this->moipFullNameAttribute;
        return substr(trim($this->$attribute), 0, 90);
    }

    public function getMoipId() : int
    {
        $attribute = $this->moipIdAttribute;
        return intval($this->$attribute);
    }

    public function getMoipEmail() : string
    {
        $attribute = $this->moipEmailAttribute;
        return substr(trim($this->$attribute), 0, 90);
    }

    public function getMoipBirthDate() : Carbon
    {
        $attribute = $this->moipBirthDateAttribute;
        return Carbon::parse($this->$attribute);
    }

    public function getMoipTaxDocument() : string
    {
        $attribute = $this->moipTaxDocumentAttribute;
        return substr(trim(str_replace(['-', '.'], '', $this->$attribute)), 0, 11);
    }

    public function getMoipShippingAddress() : AddressInterface
    {
        $attribute = $this->moipShippingAddressAttribute;
        return $this->$attribute;
    }

    public function getMoipBillingAddress() : AddressInterface
    {
        $attribute = $this->moipBillingAddressAttribute;
        return $this->$attribute;
    }

    public function getMoipPhone() : PhoneInterface
    {
        $attribute = $this->moipPhoneAttribute;
        return $this->$attribute;
    }

    final public function getMoip() : Customer
    {
        return $this->moipCustomer;
    }

    final public function saveMoip() : Customer
    {
        $data['cache'] = json_encode($this->createMoip());
        $data['code'] = json_decode($data['cache'])->id;
        $customer = Customer::make($data);
        $this->moipCustomer()->save($customer);

        return $customer;
    }

    final protected function createMoip() : \Moip\Resource\Customer
    {
        try {
            $customer = Moip::customers()->setOwnId(uniqid())
                ->setFullname($this->getMoipFullName())
                ->setEmail($this->getMoipEmail())
                ->setBirthDate($this->getMoipBirthDate()->format('Y-m-d'))
                ->setTaxDocument($this->getMoipTaxDocument())
                ->setPhone($this->getMoipPhone()->getMoipAreaCode(), $this->getMoipPhone()->getMoipPhoneNumber())
                ->addAddress('BILLING',
                    $this->getMoipBillingAddress()->getMoipStreet(),
                    $this->getMoipBillingAddress()->getMoipStreetNumber(),
                    $this->getMoipBillingAddress()->getMoipDistrict(),
                    $this->getMoipBillingAddress()->getMoipCity(),
                    $this->getMoipBillingAddress()->getMoipState(),
                    $this->getMoipBillingAddress()->getMoipZip(),
                    $this->getMoipBillingAddress()->getMoipComplement())
                ->addAddress('SHIPPING',
                    $this->getMoipShippingAddress()->getMoipStreet(),
                    $this->getMoipShippingAddress()->getMoipStreetNumber(),
                    $this->getMoipShippingAddress()->getMoipDistrict(),
                    $this->getMoipShippingAddress()->getMoipCity(),
                    $this->getMoipShippingAddress()->getMoipState(),
                    $this->getMoipShippingAddress()->getMoipZip(),
                    $this->getMoipShippingAddress()->getMoipComplement())
                ->create();
        } catch (\Exception $e) {
            dd($e);
        }

        return $customer;
    }

    final protected function moipCustomer()
    {
        return $this->morphOne(Customer::class, 'customerable');
    }
}
